/* Suitescript v1 */
function onSaveUpdateRecord(){
    setDefaultWeights();
}

function citesChecker(value) {
  var restricted = ["rosewood","pearl","abalone"];
  var fieldTextArr = value.split(" ");
  for (var i=0; i < restricted.length; i++) {
    if (value.indexOf(restricted[i]) > -1) {
      alert("Restricted Stuff!");
      nlapiSetFieldValue('custitem_cites_flag',true);
      nlapiSubmitRecord(record, true);
      return true;
    }
  }
  alert("All Clear!");
  nlapiSubmitRecord(record, true);
  return true;
}

function setDefaultWeights() {
    var record = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
    var shipCodes = {
        3: {
          "weight": 52
        } ,
        4: {
          "weight": 48
        } ,
        5: {
          "weight": 24
        } ,
        6: {
          "weight": 23
        } ,
        7: {
          "weight": 17
        } ,
        8: {
          "weight": 19
        }
    };
    var shipMethod = nlapiGetFieldValue('custitem_shipping_code');
    var itemWeight = nlapiGetFieldValue('weight');

    if (itemWeight != '' && !shippingOverride(shipMethod)) {
      	return true;
    } else {
        nlapiSetFieldValue('weight', shipCodes[shipMethod].weight);
    }
}

function shippingOverride(value) {
  var dimensionallyWeighted = [3, 4, 5, 6, 7, 8];
  for (var i = 0; i < dimensionallyWeighted.length; i++) {
    if (value.indexOf(dimensionallyWeighted[i]) > -1) {
      return true;
    }
  }
  return false;
}