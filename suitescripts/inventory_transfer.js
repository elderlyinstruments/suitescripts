/**
@NApiVersion 2.0
@NScriptType ScheduledScript
@MNModuleScope SameAccount
*/

define(["N/search"], function(s){
    function execute(context) {
        var search = s.load({
            id: "customsearch_back_order_inventory_search"
        });
        var resultSet = search.run();
        try {
            resultSet.each(printItemRecord);
        } catch(e) {
            console.log(e);
        }    
    }

    function printItemRecord(result) {
        log.debug({
            title: "Item Number",
            details: result.getValue({
                name: "itemid"
            })
        });
        return true;
    }

    return {
        execute: execute
    };
});