/**
@NApiVersion 2.0
@NScriptType ClientScript
@NModuleScome SameAccount
*/

/*Current Status is off; this breaks on "create" record */
define(["N/ui/message","N/record"], function(message, rec){
    function onSaveRecord(context) {
        if (context.mode !== "create") {
            var currentRecord = context.currentRecord;
            setItemWeightbyShippingCode(currentRecord);
        }
        return;
    };

    function setItemWeightbyShippingCode(record){

        var itemWeight = record.getValue({
            fieldId: "weight"
        });
        var recordShippingCode = record.getValue({
            fieldId: "custitem_shipping_code"
        });
        var shipCodes = {
            3: {
              "weight": 52
            } ,
            4: {
              "weight": 48
            } ,
            5: {
              "weight": 24
            } ,
            6: {
              "weight": 23
            } ,
            7: {
              "weight": 17
            } ,
            8: {
              "weight": 19
            }
        };


        if (itemWeight != '' && !shippingOverride(recordShippingCode)) {
            return true;
        } else {
            record.setValue({
                fieldId: "weight",
                value: shipCodes[recordShippingCode].weight
            });
            return true;
        }
    };

    function shippingOverride(value) {
      var dimensionallyWeighted = [3, 4, 5, 6, 7, 8];
      for (var i = 0; i < dimensionallyWeighted.length; i++) {
        if (value.indexOf(dimensionallyWeighted[i]) > -1) {
          return true;
        }
      }
      return false;
    }

    return {
        saveRecord: onSaveRecord
    };
});