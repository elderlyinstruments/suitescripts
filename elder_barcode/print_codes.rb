images = Dir["*.png"]

open('barcodes.html','a') do |f|
	f << "<div id=\"container\">\n"
end

images.each do |image|
	file_path = image
	filename = image.chomp('.png')
	open('barcodes.html','a') do |f|
		f << "\t<div class=\"barcode-label\">\n"
		f << "\t\t<img src=\"#{file_path}\"/>\n"
		f << "\t\t<p>#{filename}</p>\n"
		f << "\t</div>\n"
	end
end

open('barcodes.html','a') do |f|
	f << "</div>"
end
