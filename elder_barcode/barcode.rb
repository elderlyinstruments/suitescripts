require "CSV"
require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/png_outputter'
require 'barby/outputter/html_outputter'

# codes = ["receiving","11JF"]

CSV.foreach("2018-02-24_bins.csv") do |row|
	row.each do |code|
		barcode = Barby::Code128B.new(code)
		File.open("#{code}.png",'w'){ |f|
			f.write barcode.to_png(height: 60)
		}
	end
end







